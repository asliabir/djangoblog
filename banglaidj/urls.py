from django.contrib import admin
from django.urls import path
from blog_post import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('home/', views.home, name='home'),
    path('post-list/', views.post_list, name='post-list'),
]
